#![crate_name = "datom"]
#![crate_type = "rlib"]

use {
  serde::{ Serialize, Deserialize },
  bincode,
  blake3,
};

#[derive( Serialize, Deserialize )]
pub struct Datom {
  strok: Strok,
  esyns: Esyns,
}

#[derive( Serialize, Deserialize )]
pub struct Esyns {
  algo: Algo,
  hac: Hac,
}

#[derive( Serialize, Deserialize )]
pub struct Hac ([u8; 32]);

#[derive( Serialize, Deserialize )]
pub enum Algo {
  blake2,
  blake3,
}

#[derive( Serialize, Deserialize )]
struct Strok {
  konteks: Konteks,
  saiz: Saiz,
}

#[derive( Serialize, Deserialize )]
struct Konteks ( bool );

#[derive( Serialize, Deserialize )]
struct Saiz {
  magnytiud: Magnytiud,
  subSaiz: SubSaiz
}

#[derive( Serialize, Deserialize )]
enum Magnytiud {
  maikro,
  midiym,
  mega,
}

#[derive( Serialize, Deserialize )]
struct SubSaiz ( usize );
